# ---------------------------------------------------------------------------------------------------- #
#                               Make any changes to the code below this line.                          #
# ---------------------------------------------------------------------------------------------------- #
# ------------------------------------------ I M P O R T S ------------------------------------------- #                                                 #

from rich import print, box
from rich.align import Align as align
from rich.panel import Panel as panel
from rich.text import Text as text
from rich.console import Console as console
from rich.table import Table as table
from rich.layout import Layout as layout
from rich.live import Live as live
from rich.prompt import Prompt as prompt
from rich.progress import track as track
from rich.theme import Theme as theme
from rich.tree import Tree as tree

from transformers import AutoTokenizer, AutoModelForSequenceClassification, pipeline, AutoModelForTokenClassification, AutoModelForQuestionAnswering, AutoModelForCausalLM, AutoConfig, LlamaForCausalLM, LlamaTokenizer, LlamaConfig
import os, re, json, random, time, datetime, sys, gc, psutil, math, pickle, logging, warnings, subprocess, webbrowser, urllib, urllib.request, urllib.parse, requests, html, base64, hashlib, shutil, glob, zipfile, tarfile, tempfile, itertools, functools, collections, multiprocessing, threading, concurrent, asyncio, concurrent.futures, queue
import datasets, torch, argparse
import numpy as np
import pandas as pd

# ---------------------------------------- V A R I A B L E S ----------------------------------------- #                                            #


# Model:

MODEL_NAME = ""
FINAL_MODEL_NAME = ""


# Dataset(s):

DATASET = [
        "",
]

# Directories:

MODEL_TYPE = ""
OUTPUT_DIR = ""
CACHE_DIR = ""


# -------------------------------------------- S E T U P --------------------------------------------- #

for dir in [OUTPUT_DIR, CACHE_DIR]:
    if not os.path.exists(dir):
        os.makedirs(dir)

if MODEL_TYPE == "llama":
    TT = LlamaTokenizer
    MT = LlamaForCausalLM
    CT = LlamaConfig
else:
    TT = AutoTokenizer
    MT = AutoModelForCausalLM
    CT = AutoConfig

config = CT.from_pretrained(MODEL_NAME)
tokenizer = TT.from_pretrained(MODEL_NAME)
model = MT.from_pretrained(MODEL_NAME, config, cache_dir=CACHE_DIR if CACHE_DIR else os.path.join(OUTPUT_DIR, "cache"))

bnbc = BitsAndBytesConfig(
    load_in_4bit=True,
    bnb_4bit_use_double_quant=True,
    bnb_4bit_quant_type="nf4",
    bnb_4bit_compute_dtype=torch.bfloat16,
)

quantization_config = torch.quantization.get_default_qconfig("fbgemm")


OUTP = os.path.join(OUTPUT_DIR, FINAL_MODEL_NAME)
INM = MODEL_NAME
OUTM = FINAL_MODEL_NAME

parser = argparse.ArgumentParser()
parser.add_argument("action", help="The action to perform. Options: train, quantize, convert, convert_to_bnb, convert_to_bnb_quant, convert_to_bnb_quant_4bit")
args = parser.parse_args()

# ---------------------------------------- F U N C T I O N S ----------------------------------------- #

def recursive_tokenize(data, tokenizer, detected_texts=[]):

    if isinstance(data, str):
        detected_texts.append(data)

    elif isinstance(data, dict):
        for value in data.values():
            recursive_tokenize(value, tokenizer, detected_texts)

    elif isinstance(data, list):
        for item in data:
            recursive_tokenize(item, tokenizer, detected_texts)

    return detected_texts

def custom_mapper(samples, tokenizer):
    texts_to_tokenize = recursive_tokenize(samples, tokenizer)
    return tokenizer(*texts_to_tokenize)

dataset = DATASET.map(lambda samples: custom_mapper(samples, tokenizer))


def train():
    model.train()
    for epoch in range(1):
        for batch in dataset:
            model(**batch)
    model.save_pretrained(OUTP)

def quantize():
    model = MT.from_pretrained(INM, config, cache_dir=CACHE_DIR if CACHE_DIR else os.path.join(OUTPUT_DIR, "cache"))
    model.load_state_dict(torch.load(os.path.join(OUTP, "pytorch_model.bin")))
    model = torch.quantization.quantize_dynamic(model, {torch.nn.Linear}, dtype=torch.qint8)
    model.save_pretrained(OUTP)

def convert():
    model = MT.from_pretrained(INM, config, cache_dir=CACHE_DIR if CACHE_DIR else os.path.join(OUTPUT_DIR, "cache"))
    model.load_state_dict(torch.load(os.path.join(OUTP, "pytorch_model.bin")))
    model = model.half()
    model.save_pretrained(OUTP)

def convert_to_bnb():
    model = MT.from_pretrained(INM, config, cache_dir=CACHE_DIR if CACHE_DIR else os.path.join(OUTPUT_DIR, "cache"))
    model.load_state_dict(torch.load(os.path.join(OUTP, "pytorch_model.bin")))
    model = model.to_bnb(bnbc)
    model.save_pretrained(OUTP)

def convert_to_bnb_quant():
    model = MT.from_pretrained(INM, config, cache_dir=CACHE_DIR if CACHE_DIR else os.path.join(OUTPUT_DIR, "cache"))
    model.load_state_dict(torch.load(os.path.join(OUTP, "pytorch_model.bin")))
    model = model.to_bnb(bnbc)
    model = torch.quantization.quantize_dynamic(model, {torch.nn.Linear}, dtype=torch.qint8)
    model.save_pretrained(OUTP)

def convert_to_bnb_quant_4bit():
    model = MT.from_pretrained(INM, config, cache_dir=CACHE_DIR if CACHE_DIR else os.path.join(OUTPUT_DIR, "cache"))
    model.load_state_dict(torch.load(os.path.join(OUTP, "pytorch_model.bin")))
    model = model.to_bnb(bnbc)
    model = torch.quantization.quantize_dynamic(model, {torch.nn.Linear}, dtype=torch.qint4)
    model.save_pretrained(OUTP)

def convert_to_bnb_quant_4bit_double():
    model = MT.from_pretrained(INM, config, cache_dir=CACHE_DIR if CACHE_DIR else os.path.join(OUTPUT_DIR, "cache"))
    model.load_state_dict(torch.load(os.path.join(OUTP, "pytorch_model.bin")))
    model = model.to_bnb(bnbc)
    model = torch.quantization.quantize_dynamic(model, {torch.nn.Linear}, dtype=torch.qint4, qconfig=quantization_config)
    model.save_pretrained(OUTP)


# -------------------------------------------- M A I N ---------------------------------------------- #

if args.action == "train":
    train()
elif args.action == "quantize":
    quantize()
elif args.action == "convert":
    convert()
elif args.action == "convert_to_bnb":
    convert_to_bnb()
elif args.action == "convert_to_bnb_quant":
    convert_to_bnb_quant()
elif args.action == "convert_to_bnb_quant_4bit":
    convert_to_bnb_quant_4bit()
elif args.action == "convert_to_bnb_quant_4bit_double":
    convert_to_bnb_quant_4bit_double()
else:
    raise Exception("Unknown action: " + args.action)

